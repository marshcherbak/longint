#pragma once
#include <string>
#include <vector>
using namespace std;
class LongInt {
public:
	LongInt();
	LongInt(string);
	LongInt(vector <int> nnum, bool s);
	LongInt(int val);
	LongInt operator * (LongInt &other);
	LongInt operator + (LongInt &other);
	LongInt operator +(int val);
	LongInt operator *(int val);
	LongInt operator - (LongInt &other);
	LongInt operator - (int val);
	LongInt operator / (LongInt &other);
	LongInt operator / (int val);
	LongInt operator = (LongInt &other);	
	bool operator ==(LongInt &other);
	bool operator ==(int val);
	bool operator !=(int val);
	bool operator !=(LongInt &other);
	bool operator > (LongInt &other);
	bool operator >(int val);
	bool operator < (LongInt &other);
	bool operator >=(LongInt &other);
	bool operator <=(LongInt &other);
	LongInt operator%(LongInt &other);
	LongInt operator%(int val);

	LongInt GCD(LongInt &other);
	LongInt longpow(LongInt &other, LongInt &m);
	LongInt Jakobi(LongInt &b);
	LongInt longRand();

	LongInt Karatsuba(LongInt &other);
	bool SolovayStrassen(long long t);
	bool RabinMiller(int t);
	bool Lehmann(long long t);

	int evenOutSizes(LongInt&other);
	void deleteEmptyEl();

	bool odd_num();
	friend ostream& operator<<(ostream & os, const LongInt & l);

private:
	vector <int> num;
	static int mult;
	const int base = 1000;
	const int b_st = 3;
	bool sign; // 1 + , 0 - 
};

