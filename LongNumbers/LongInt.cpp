﻿#include "LongInt.h"
#include <string>
#include <iostream>
//#include <algorithm>
using namespace std;


LongInt::LongInt():sign(1){}

LongInt::LongInt(string s)
{
	int n;
	int l = strlen(s.c_str());
	if (l % b_st == 0)
		n = l / b_st;
	else
		n = l / b_st + 1;
	
	int firsts = l%b_st;
	string k;
	int j = 0;
	for (int i = l - b_st; i >= firsts && j < n; i = i - b_st, ++j) {
			k = s.substr(i, b_st);
		num.push_back(stoi(k));
	}
	if (j != n) {
		k = s.substr(0, firsts);
		num.push_back(stoi(k));
	}
	sign = 1;
}

LongInt::LongInt(vector<int> nnum, bool s)
{
	for (int i = 0; i < nnum.size(); ++i)
		num.push_back(nnum[i]);
	sign = s;
}

LongInt::LongInt(int val)
{

	string s = to_string(val);
	LongInt A(s);
	*this = A;
	sign = (val >= 0);
}

LongInt LongInt::operator * (LongInt &other)
{
	int s = num.size() + other.num.size();
	int max_parts = (num.size()+other.num.size())*other.num.size();
	int second = other.num.size();
	vector<int> res;// = new int[s];
	vector<int> parts;// = new int[max_parts];
	int t=0, ost = 0, k = 0, m;

	for (int i = 0; i < other.num.size(); ++i) {
		for (int j = 0; j < num.size(); ++j) {
			t = num[j] * other.num[i] + ost;
			ost = t / base;
			if (t >= base) {
				t = t % base;
			}
			parts.push_back(t);
			k++;
		}
		parts.push_back(ost);
		if (parts.size() != (max_parts)) {
			if (i != (second - 1)) {
				m = parts.size()+1;
				for (int k=m; k < m + second; ++k)
					parts.push_back(0);
			}
		}
		ost = 0;
	}
	t = 0;
	m = 0;
	for (int i = 0; i < s; ++i) {
		t = parts[i];
		for (int j = 0, x = s; j < second-1; j++) {
			t += parts[i + x];
			x +=s;
		}
		t += ost;
		ost = t / base;
		if (t >= base)
			t = t % base;
		res.push_back(t);
	}
	LongInt result(res,1);
	result.deleteEmptyEl();
	return result;
}

LongInt LongInt::operator+(LongInt & other)
{
	int a = evenOutSizes(other);
	vector<int>res;
	
	bool s = (sign == other.sign);
	int t, ost = 0;
	if (s) {
		s = sign;
		for (int i = 0; i < a; ++i) {
			t = num[i] + other.num[i] + ost;
			ost = t / base;
			if (t >= base)
				t = t % base;
			res.push_back(t);
		}
		if (ost != 0)
			res.push_back(ost);
	LongInt result(res, s);
	result.deleteEmptyEl();
	return result;
	}
	else {
		if (sign) {
			LongInt oPlus = other; oPlus.sign = 1;
			return *this - oPlus;
		}
		else {
			LongInt tPlus = *this; tPlus.sign = 1;
			return other - tPlus;
		}
	}
	
}

LongInt LongInt::operator+(int val)
{
	vector<int> a;
	a.push_back(val);
	LongInt A(a, 1);
	return *this + A;
}

LongInt LongInt::operator*(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this)*A;
}

LongInt LongInt::operator-(LongInt & other)
{
//	bool s;
	int a = evenOutSizes(other);
	vector<int> res;

	if (sign && other.sign) {

		if (other > *this) {
			LongInt res = other - *this;
			res.sign = 0;
			return res;
		}
		else {
			for (int i = 0; i < a; ++i) {
				if (num[i] < other.num[i]) {
					num[i] += base;
					num[i + 1]--;
				}
				res.push_back(num[i] - other.num[i]);
			}
			LongInt result(res, 1);
			result.deleteEmptyEl();
			return result;
		}				
	}

	else if (sign && !other.sign) {
		LongInt t = other; t.sign = 1;
		return *this + t;
	}

	else if (!sign && sign) {
		LongInt t = *this; t.sign = 1;
		t = t + other; 
		return t;
	}
	else {
		LongInt t1 = *this; t1.sign = 1;
		LongInt t2 = other; t2.sign = 1;
		t1 = t2 - t1;
		return t1;
	}

}

LongInt LongInt::operator-(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this)-A;
}

LongInt LongInt::operator/(LongInt & other)
{
	LongInt A = *this, B = other;
	B.deleteEmptyEl();
	int a, b;
	LongInt zero("0");
	if ((other > A) || ((A == zero) && !(B ==zero)))
		return LongInt("0");
	if ((B == zero) && !(A == zero))
		throw exception("error: division by zero\n");
	if ((B == zero) && (A == zero))
		throw exception("NaN\n");
	if (other == A)
		return LongInt("1");
	LongInt res;
	b = B.num.size();
	do {
		B.deleteEmptyEl();
		LongInt A1;
		a = A.num.size();
		int i, k = 0;
		for (i = a-b; i < a; ++i)
			A1.num.push_back(A.num[i]);
		if (A1 < B) {
			A1.num.insert(A1.num.begin(), A.num[a - b - 1]);
			k = 1;
		}

		int C=0;
		bool flag = 1;
		for (int i = 1; i < base && flag; ++i) {
			if (B * i <= A1)
				C = i;
			else
				flag = false;
		}
		res.num.push_back(C);
		LongInt t = B * C;
		t.num.insert(t.num.begin(), a-b-k, 0);
		A = A - t;
	} while (A >= B);
	reverse(res.num.begin(), res.num.end());
	if ((B * res) + A < (*this))
		res.num.insert(res.num.begin(), 0);

	return res;
}

LongInt LongInt::operator/(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this)/A;
}

LongInt LongInt::operator=(LongInt & other)
{
	num.swap(other.num);
	sign = other.sign;
	return *this;
}

bool LongInt::operator==(LongInt & other)
{
	if (num.size() != other.num.size())
		return false;
	else {
		for (int i = 0; i < num.size(); ++i)
			if (num[i] != other.num[i])
				return false;
	}
	return true;
}

bool LongInt::operator==(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this) == A;
	
}

bool LongInt::operator!=(int val)
{
	return !(*this==val);
}

bool LongInt::operator!=(LongInt & other)
{
	return !(*this == other);
}

bool LongInt::operator>(LongInt & other)
{
	if (sign && !sign)
		return true;
	int a = evenOutSizes(other);
	bool isGreater = 0;
	bool stop=0;
	int i = a-1;
	for(int i = a-1; i>=0 && !stop; --i){
		if (num[i] > other.num[i]) {
			isGreater = 1;
			stop = 1;
		}
		else if (num[i] < other.num[i]) {
			isGreater = 0;
			stop = 1;
		}
	}
	return isGreater;
}

bool LongInt::operator>(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this) > A;
}

bool LongInt::operator<(LongInt & other)
{
	return (!(*this > other) && !(*this==other));
}

bool LongInt::operator>=(LongInt & other)
{
	return ((*this > other) || (*this == other));
}

bool LongInt::operator<=(LongInt & other)
{
	return ((*this < other) || (*this == other));
}

LongInt LongInt::operator%(LongInt & other)
{
	LongInt res = (*this) / other;
	res = (*this) - (res*other);
	return res;
}

LongInt LongInt::operator%(int val)
{
	string s = to_string(val);
	LongInt A(s);
	return (*this)%A;
}

int LongInt::evenOutSizes(LongInt & other)
{
	int a;
	if (num.size() == other.num.size())
		return num.size();
	if (num.size() > other.num.size()) {
		a = num.size();
		for (int i = other.num.size(); i < a; ++i)
			other.num.push_back(0);
		//other.n = a;
	}
	else {
		a = other.num.size();
		for (int i = num.size(); i < a; ++i)
			num.push_back(0);
		//n = a;
	}
	return a;
}

void LongInt::deleteEmptyEl()
{
	int k = num.size() - 1;
	for (int i = k; i > 0; --i) {
		if (num[i] == 0)
			num.erase(num.begin() + i);
		else
			break;
	}
}

bool LongInt::odd_num()
{
	if (num.back() % 2 == 0)
		return false;
	else
		return true;
}

LongInt LongInt::GCD(LongInt & other)
{
	LongInt a = *this;
	LongInt b = other;
	while (a != 0 && b != 0) {
		if (a > b)
			a = a%b;
		else
			b = b%a;
		a.deleteEmptyEl();
		b.deleteEmptyEl();
	}
	return a + b;
}

LongInt LongInt::longpow(LongInt & power, LongInt &m)
{
	LongInt i(1);
	LongInt A = *this;
	LongInt pow = *this;
	for (; i < power; i = i + 1) {
		if (m != 1) {
			A = (pow*A) % m;
		}
		else
			A = A*pow;
	}
	return A;
}

LongInt LongInt::Jakobi(LongInt &b)
{
	LongInt a = *this;

	if (a.GCD(b) != 1)
		return 0;

	int r = 1;

	if (a.sign == 0) {
		a.sign = 1;
		if (b % 4 == 3)
			r = -r;
	}

	
	while (a != 0) {
		LongInt t(0);
		while (!(a.odd_num())) {
			t = t +  1;
			a = a / 2;
		}

		if (t % 2 != 0) {
			if ((b % 8 == 3) || (b % 8 == 5))
				r = -r;
		}

		if ((a % 4 == 3) && (b % 4 == 3))
			r = -r;
		LongInt c;
		c = a;
		a = b % c;
		b = c;
	}
	LongInt R(r);
	return R;
}

LongInt LongInt::longRand()
{
	LongInt x(0);
	LongInt m(1451);
	LongInt a(25);
	LongInt c(32);
	x = (a*x + c) % m;
	return x;
}

LongInt LongInt::Karatsuba(LongInt & other)
{
	LongInt res;
	bool s = 1;
	if (sign != other.sign)
		s = 0;
	//вирівнюємо довжини
	const int z = 0;
	int k = evenOutSizes(other);

	if (k == 1)
		return *this * other;

	if (k % 2 != 0) {
		k++;
		num.push_back(z);
		other.num.push_back(z);
	}

	LongInt tLeft, tRight, oLeft, oRight, sum1, sum2, p1, p2, p3, p4;
	int nd2 = k / 2;
	LongInt t;

	for (int i = 0; i < nd2; ++i) {
		tLeft.num.push_back(num[i]);
		oLeft.num.push_back(other.num[i]);
	}
	for (int i = nd2; i < k; ++i) {
		tRight.num.push_back(num[i]);
		oRight.num.push_back(other.num[i]);
	}

	sum1 = tLeft + tRight;
	sum2 = oLeft + oRight;
	p1 = tLeft.Karatsuba(oLeft);
	p2 = tRight.Karatsuba(oRight);
	p3 = sum1.Karatsuba(sum2);
	p4 = p3 - (p1 + p2);


	p1.deleteEmptyEl();
	p2.deleteEmptyEl();
	p4.deleteEmptyEl();
	auto t4 = p4.num.begin();
	p4.num.insert(t4, nd2, 0);
	auto t2 = p2.num.begin();
	p2.num.insert(t2, k, 0);

	res = p1 + p2 + p4;
	res.sign = s;
	return res;
}

bool LongInt::SolovayStrassen(long long t)
{
	LongInt p = *this;
	if (p % 2 == 0)
		return false;
	for (int i = 0; i < t; ++i) {
		LongInt a = longRand()%(*this-5)+6;
		if (p.GCD(a) != 1)
			return false;
		LongInt power = (p - 1) / 2;
		LongInt j = a.longpow(power,p);
		LongInt J = p.Jakobi(a);
		if (j != J)
			return false;
	}
	return true;
}

bool LongInt::RabinMiller(int t)
{
	LongInt p = *this;
	LongInt two(2);
	LongInt one(1);
	if (p % 2 == 0)
		return false;
	for (int i = 0; i < t; ++i) {
		int B=1;
		LongInt div = (p - 1) / 2;
		while (div != 0) {
			div = div/2;
			B++;
		}
		LongInt b(B-1);
		LongInt m = (p - 1) / two.longpow(b, one);
		LongInt a = longRand()%(p-3)+2;
		LongInt j(0);
		LongInt z = (a*m)%p;
		if (z == 1 || z == p - 1)
			continue;
		for (; j < b-1; j = j + 1) {
			z = (z*z)%p;
			if (z == 1)
				return false;
			if (z == p - 1)
				break;
		}
		if (j == b && z != p - 1)
			return false;
	}
	return true;
}

bool LongInt::Lehmann(long long t)
{
	LongInt p = *this;
	if (p % 2 == 0)
		return false;
	for (int i = 0; i < t; ++i) {
		LongInt a = longRand() % (*this - 5) + 4;
		LongInt power = (p - 1) / 2;
		LongInt j = a.longpow(power,p);
		j.deleteEmptyEl();
		p.deleteEmptyEl();
		//j = j % p;
		if (j != 1 && j != (p - 1))
			return false;
	}
	return true;
}


ostream & operator<<(ostream & os, const LongInt & l)
{
	int k = l.num.size() - 1;
	for (int i = k; i >= 0; --i) {
		os.width(l.b_st);
		os.fill('0');
		os << l.num[i] << " ";
	}
	return os;
}
