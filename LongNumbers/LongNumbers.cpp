#include <iostream>
#include <string>
#include "LongInt.h"
using namespace std;

int main() {
	//12345678912345679968096808690546905689
	//365406548546404654044
	string s1, s2;
	cout << "Enter string 1: ";
	cin >> s1;
	cout << "\nEnter string 2: ";
	cin >> s2; 

	LongInt a1(s1);
	LongInt a2(s2);

	cout << "\n\n1. Usual multiplication:     " << a1 << "* " << a2 << "= " << a1 * a2;
	a1.deleteEmptyEl(); 
	a2.deleteEmptyEl();
	cout << "\n\n2. Karatsuba multiplication: " << a1 << "* " << a2 << "= ";
	cout << a1.Karatsuba(a2);
	a1.deleteEmptyEl(); 
	a2.deleteEmptyEl();

	cout << "\n\n3. Long division:            " << a1 << "/ " << a2 << "= ";
	try {
		cout << a1 / a2;
	}
	catch (exception e) {
		cout << e.what() << endl;
	}
	
	string prime, noprime;
	cout << "\n\nPrime: ";
	cin >> prime;


	LongInt p1(prime);
	if (p1.RabinMiller(1))
		cout << "\n The number is prime. RM\n";
	else
		cout << "\n Not prime.RM\n";


	cout << endl << endl;
	system("pause");
	return 0;

}